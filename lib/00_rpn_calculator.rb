class RPNCalculator
  def initialize
    @calc = []
  end

  OPS = [:+, :-, :*, :**, :/, :|, :&]

  def push(num)
    @calc << num
  end

  def plus
    enter(:+)
  end

  def minus
    enter(:-)
  end

  def times
    enter(:*)
  end

  def divide
    enter(:/)
  end

  def value
    @calc.last
  end

  private

  def enter(oper)
    raise "calculator is empty" unless @calc.length >= 2
    last = @calc.pop
    first = @calc.pop

    if oper == :+
      @calc << first + last
    elsif oper == :-
      @calc << first - last
    elsif oper == :*
      @calc << first * last
    elsif oper == :/
      @calc << (first * 1.0) / (last * 1.0)
    else
      @calc << first << last
      raise ArgumentError.new("No #{oper}")
    end

    self

  end

end
